//
//  AaronCard2App.swift
//  AaronCard2
//
//  Created by  Aaron  on 10/20/21.
//

import SwiftUI

@main
struct AaronCard2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
